# AI_minor_OP4
Ai Minor period 4 - Deep Learning

To run realtime detection:
Set pythonpath:

  `export PYTHONPATH=$PYTHONPATH:``pwd``:``pwd``/slim`

Run the python program from directory *'AI_minor_OP4/tensorflow_api'*:

  `python object_detection/detect_stream.py`
